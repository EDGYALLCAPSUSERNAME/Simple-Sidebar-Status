from bs4 import BeautifulSoup
import requests
import praw
import time

REDDIT_USERNAME = ''
REDDIT_PASS = ''
SUBREDDIT_NAME = ''

SERVICE_STATUS = {}

SERVICE_TEMPLATE = "|[{}](https://status.simple.com/)|[{}](#{})|\n"

# Add what you want to go above the table here
# End each line with \n
SIDEBAR_TOP = """

"""

# Add what you want to go below the table here
# End each line with \n
SIDEBAR_BOTTOM = """

"""


def get_service_status(soup):
    for service in soup.find_all('div', class_='panel'):
        SERVICE_STATUS[str(service.find('h6').getText())] = str(service['data-status'])


def update_sidebar(r, sub):
    services_sidebar_string = "|Service|Status|\n:----|---:\n"
    for key, value in SERVICE_STATUS.iteritems():
        services_sidebar_string += SERVICE_TEMPLATE.format(key,
                                                           value.capitalize(),
                                                           value)

    sidebar_string = SIDEBAR_TOP + services_sidebar_string + SIDEBAR_BOTTOM

    r.update_settings(sub, description=sidebar_string)


def main():
    print "Getting the status page..."
    request = requests.get('https://status.simple.com/')
    data = request.text
    soup = BeautifulSoup(data, "html.parser")
    r = praw.Reddit(user_agent='Simple_Bank_Status_Sidebar v1.0')

    print "Logging in..."
    r.login(REDDIT_USERNAME, REDDIT_PASS, disable_warning=True)
    sub = r.get_subreddit(SUBREDDIT_NAME)

    while True:
        print "Gettings the statuses..."
        get_service_status(soup)
        print "Updating sidebar..."
        update_sidebar(r, sub)
        print "Sleeping..."
        # Sleeps for 30 minutes
        time.sleep(1800)


if __name__ == '__main__':
    main()
