Simple Bank Sidebar Status
==========================

Setup
-----

This script requires python 2.7, praw, requests, and beautiful soup.

To install praw, requests, and beautiful soup type in your command line:

    pip install praw

    pip install requests

    pip install beautifulsoup4


There are two variables SIDEBAR_TOP and SIDEBAR_BOTTOM, add what you want to go above and below
the table of statuses inbetween the """.

Finally, add your username and password and the subreddit you moderate that you want to add the table to.

Included is a style.css file that provides basic styling that you can expand on if you wish.
